﻿using DataSerializerDeserializer.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataSerializerDeserializer.BussinessLogic
{
   public class DataWarehouse
    {
        public static List<Question> Questions { get; set; } = new List<Question>();
        public static List<Answer> Answers { get; set; } = new List<Answer>();
       
    }
}
