﻿using DataSerializerDeserializer.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataSerializerDeserializer.BussinessLogic
{
   public class JsonSerializer
   {
    public void SerializeQuestions(List<Question> questions, string filePath )
        {
            var jsonData = JsonConvert.SerializeObject(questions, Formatting.Indented);
            File.WriteAllText(filePath, jsonData);
        }
    public List<Question> DeserializeQuestions (string filePath)
        {
            var jsonData = File.ReadAllText(filePath);
           var questions = JsonConvert.DeserializeObject<List<Question>>(jsonData);
            return questions;
        }
    public void SerializeAnswers(List<Answer> answers, string filePath)
        {
            var jsonData = JsonConvert.SerializeObject(answers, Formatting.Indented);
            File.WriteAllText(filePath, jsonData);
        }
        public List<Answer> DeserializeAnswers(string filePath)
        {
            var jsonData = File.ReadAllText(filePath);
            var answers = JsonConvert.DeserializeObject<List<Answer>>(jsonData);
            return answers;
        }
    }
}
