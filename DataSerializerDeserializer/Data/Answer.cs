﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataSerializerDeserializer.Data
{
    public class Answer
    {
        public int Idquestion { get; set; } 
        public string AnswerValue { get; set; }
        public int Count { get; set; }
    }
}
