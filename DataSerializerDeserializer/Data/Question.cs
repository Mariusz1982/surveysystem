﻿using System;
using System.Collections.Generic;

namespace DataSerializerDeserializer.Data
{
    public class Question
    {
        public int Id { get; set; }
        public string  QuestionValue { get; set; }
        public List<Answer> OwnedAnswers { get; set; }


    }
}
