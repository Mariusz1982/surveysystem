﻿using DataSerializerDeserializer.BussinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
namespace Reporter
{
    class Program
    {
        private readonly JsonSerializer _jsonSerializer = new JsonSerializer();
        static void Main(string[] args)
        {
            new Program().Run();

        }

        private void Run()
        {
            
        var DatFromAnswers = _jsonSerializer.DeserializeAnswers(@"./answers.json");
            foreach (var answers in DatFromAnswers)
            {
                Console.WriteLine(answers.Idquestion);

            }

        }

    }
}
