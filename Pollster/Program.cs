﻿using DataSerializerDeserializer.BussinessLogic;
using DataSerializerDeserializer.Data;
using System;
using System.Collections.Generic;
using System.IO;
namespace Pollster
{
    class Program
    {
        private readonly JsonSerializer _jsonSerializer = new JsonSerializer();
        static void Main(string[] args)
        {
            new Program().Run();
            
        }
        private void Run()
        {
          
            InitializeData();
            _jsonSerializer.SerializeQuestions(DataWarehouse.Questions, @"./Data.json");


           var NewDataFromJson = _jsonSerializer.DeserializeQuestions(@"./Data.json");

            if (!File.Exists(@"./answers.json"))
            {
                _jsonSerializer.SerializeAnswers(DataWarehouse.Answers, @"./answers.json");
            }
            else
            {
                var DatFromAnswers = _jsonSerializer.DeserializeAnswers(@"./answers.json");
                foreach (var dataanswer in DatFromAnswers)
                {
                    //Console.WriteLine(dataanswer.AnswerValue);
                    string anshelper = dataanswer.AnswerValue;
                    int ide = dataanswer.Idquestion;
                    var answer1 = new Answer
                    {
                        Idquestion = ide,
                        AnswerValue = anshelper,
                       

                    };
                    DataWarehouse.Answers.Add(answer1);
                }
            }


            //DataWarehouse.Answers.Add(Answers);
            Console.WriteLine("Poniżej ankieta. Proszę o wybór po jednej odpowiedzi dla każdego pytania");
            
            foreach (var question in NewDataFromJson)
            {
               // _jsonSerializer.Serialize(DataWarehouse.Answers, @"./answers.json");

                Console.WriteLine($"{question.QuestionValue}");
                foreach (var answers in question.OwnedAnswers)
                {
                    
                    Console.WriteLine(answers.AnswerValue);

                    
                    
                }

           
                string userInput;
                int answerNumber;
                do
                {
                    Console.WriteLine("Podaj  numer odpowiedzi: ");
                    userInput = Console.ReadLine();
                } while (!int.TryParse(userInput, out answerNumber)
                         || (answerNumber<1)||answerNumber>question.OwnedAnswers.Count);

                //var serializedeserialize = new SerializeService();
                //serializedeserialize.SerialiseDeserializeMethod(@"./answers.json", answerNumber);

                string ans = question.OwnedAnswers[answerNumber - 1].AnswerValue;
                int Ident = question.Id;

                Console.WriteLine(ans);
                Console.WriteLine(Ident);



                var answer = new Answer
                {
                    Idquestion = Ident,
                    AnswerValue = ans,
                  

                };
                DataWarehouse.Answers.Add(answer);


                _jsonSerializer.SerializeAnswers(DataWarehouse.Answers, @"./answers.json");

            }

            





        }
        private void InitializeData()
        {

            var question1 = new Question
            {
                Id = 1,
                QuestionValue = "Czy koronawirus zabije wszystkich ludzi",
                OwnedAnswers = new List<Answer>
            {
                new Answer
                {
                    Idquestion=1,
                    AnswerValue="Tak zabije wszystkich"

                },
                new Answer
                {
                    Idquestion=1,
                    AnswerValue="Nie zabije wszystkich"
                    
                },
                 new Answer
                {
                     Idquestion=1,
                    AnswerValue="Nie wiem"
                    
                }
            }
            };
            var question2 = new Question
            {
                Id = 2,
                QuestionValue = "Czy kiedyś będą jeździć samochody autonomiczne",
                OwnedAnswers = new List<Answer>
            {
                new Answer
                {
                    Idquestion=2,
                    AnswerValue="Tak"
                    
                },
                new Answer
                {
                    Idquestion=2,
                    AnswerValue="Nie"
                    
                },
                 new Answer
                {
                     Idquestion=2,
                    AnswerValue="Nie wiem"
                    
                }
            }
            };
            var question3 = new Question
            {
                Id = 3,
                QuestionValue = "Czy bus pasy powodują korki w mieście",
                OwnedAnswers = new List<Answer>
            {
               new Answer
                {
                    Idquestion=3,
                    AnswerValue="Tak"
                    
                },
                new Answer
                {
                    Idquestion=3,
                    AnswerValue="Nie"
                    
                },
                 new Answer
                {
                     Idquestion=3,
                    AnswerValue="Nie wiem"
                    
                }
                 

            }
            };
            DataWarehouse.Questions.Add(question1);
            DataWarehouse.Questions.Add(question2);
            DataWarehouse.Questions.Add(question3);
           //DataWarehouse.Answers.Add(question1.OwnedAnswers[0]);
           //DataWarehouse.Answers.Add(question1.OwnedAnswers[1]);
           // DataWarehouse.Answers.Add(question1.OwnedAnswers[2]);
        }
    }
}
